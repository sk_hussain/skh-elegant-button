var gulp = require('gulp'),
    compass = require('gulp-compass'),
    plumber = require('gulp-plumber'),
    notify = require('gulp-notify'),
    autoprefixer = require('gulp-autoprefixer'),
    cssMinify = require('gulp-uglifycss'),
    jsMinify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    del = require('del'),
    zip = require('gulp-zip');

// ============================================================================
// VARIABLE FOR DIRECTORIES
var plugin_name = 'skh-elegant-button'

// ============================================================================
// ==========================================================================
// NOTIFY POPUP FOR ERRORS the title and icon that will be used for the Grunt
// notifications
var notifyInfo = {
    title: 'Gulp'
};

//error notification settings for plumber
var plumberErrorHandler = {
    errorHandler: notify.onError({title: notifyInfo.title, message: "Error: <%= error.message %>"})
};

// ==============================================================================
// PHASE A:====================== Generate CSS from SCSS for ADMIN and public
/*
1: scss_admin
2: scss_public
3: watch

*/

// PHASE B:======================
/*
1: deleteDistFolder
2: copyFiles
3: buildZip

*/

/*
**
**========================== PHASE A ==================================================
**
*/

// 1: scss_admin SCSS PROCESS USING COMPASS PLUGIN
// ==========================================
gulp.task('scss_admin', function () {
    return gulp
        .src('./sass/admin/**/*.scss')
        .pipe(plumber(plumberErrorHandler))
        .pipe(compass({css: './admin/css', sass: './sass/admin', sourcemap: true}))
        .pipe(plumber.stop())
        .pipe(gulp.dest('./admin/css'))

})

// 2: scss_public
gulp.task('scss_public', function () {
    return gulp
        .src('./sass/public/**/*.scss')
        .pipe(plumber(plumberErrorHandler))
        .pipe(compass({css: './public/css', sass: './sass/public', sourcemap: true}))
        .pipe(plumber.stop())
        .pipe(gulp.dest('./public/css'))

})

// 3: watch GULP WATCH
gulp.task('watch', function () {

    //Watching ADMIN SCSS files
    gulp.watch('./sass/admin/**/*.scss', ['scss_admin']);

    //Watching PUBLIC SCSS files
    gulp.watch('./sass/public/**/*.scss', ['scss_public']);

});

/*
**
**========================== PHASE B ==================================================
**
*/
// 1: deleteDistFolder
gulp.task('deleteDistFolder', function () {
    return del('./dist');
})

// 2: copyFiles Copy all the files to dist folder
gulp.task('copyFiles', ['deleteDistFolder'], function () {
    var pathToCopy = [
        './**/*',
        '!./.gitignore',
        '!./**/*.css.map',
        '!./gulpfile.js',
        '!./.sass-cache',
        '!./node_modules.lnk',
        '!./**/*.scss',
        '!./{sass,sass/**}'
    ]
    return gulp
        .src(pathToCopy)
        .pipe(gulp.dest('./dist/' + plugin_name))
})

// 3: buildZip
gulp.task('buildZip', ['copyFiles'], function () {
    var zipName = plugin_name + '.zip';
    return gulp
        .src('./dist/**/*')
        .pipe(zip(zipName))
        .pipe(gulp.dest('./dist'))
})

/*
==============================================================================================
1: gulp watch
2: gulp buildZip
*/