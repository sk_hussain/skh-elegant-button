=== Elegant Button ===
Contributors: Shekh Kamal
Tags: Admin css, Divi admin, Divi, builder, Modern
Requires at least: 3.0.2
Tested up to: 4.7
Stable tag: 3.5.0
License: GPL2
License URI: http://www.gnu.org/licenses/gpl-2.0.txt



== Installation ==
From your WordPress dashboard
1. Download Elegant Button Plugin.
2. Visit \'Plugins > Add New\' > \'Upload Plugin\' Button
3. Activate Elegant Button from your Plugins page.
Thats it :)

== Changelog ==
1.00
Initial Version