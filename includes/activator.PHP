<?php
/**
 * The code that runs during plugin activation.
 */

    if(version_compare(get_bloginfo('version'), '4.0.0', '<')){
        wp_die(__('Update your wordpress to use this plugin.', 'skh-plugin-slug'));
    }

