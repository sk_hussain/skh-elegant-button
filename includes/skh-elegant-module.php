<?php
class ET_Builder_Module_Elegant_Button extends ET_Builder_Module {
	function init() {
		$this->name       = esc_html__( 'Elegant Button', 'et_builder' );
		$this->slug       = 'et_pb_skh_elegant_btn';
		$this->fb_support = true;
		$this->whitelisted_fields = array(
		'background_layout',
		'text_orientation',
		'content_new',
		'admin_label',
		'module_id',
		'module_class',
		'button_text',
		'button_url',
		'button_style',
		'button_size',
		'button_border',
		'button_radius',
		'button_orientation',
		'button_theme',
		);
		$this->fields_defaults = array(
		'background_layout' => array( 'light' ),
		'text_orientation'  => array( 'left' ),
		'button_orientation'  => array( 'left' ),
		'button_theme'  => array( 'dark' ),
		);
		$this->main_css_element = '%%order_class%%';
		$this->advanced_options = array(
		'custom_margin_padding' => array(
		'css' => array(
		'important' => 'all',
		),
		),
		);
	}
	function get_fields() {
		$fields = array(

		'button_text' => array(
		'label'             => esc_html__( 'Button Text', 'et_builder' ),
		'type'              => 'text',
		'option_category'   => 'basic',
		'description'       => esc_html__( 'Text of the Button.', 'et_builder' ),
		),

		'button_url' => array(
		'label'             => esc_html__( 'Button URL', 'et_builder' ),
		'type'              => 'text',
		'option_category'   => 'basic',
		'description'       => esc_html__( 'Enter URL of the Button.', 'et_builder' ),
		),

		'button_style' => array(
		'label'             => esc_html__( 'Button Style', 'et_builder' ),
		'type'              => 'select',
		'option_category'   => 'configuration',
		'options'           => array(
		'winona' => esc_html__( 'Type 1', 'et_builder' ),
		'wapasha'  => esc_html__( 'Type 2', 'et_builder' ),
		'ujarak'  => esc_html__( 'Type 3', 'et_builder' ),
		),
		'description'       => esc_html__( 'Style of the Button.', 'et_builder' ),
		),

		'button_theme' => array(
		'label'             => esc_html__( 'Button Theme', 'et_builder' ),
		'type'              => 'select',
		'option_category'   => 'configuration',
		'options'           => array(
		'dark' => esc_html__( 'Dark', 'et_builder' ),
		'light'  => esc_html__( 'Light', 'et_builder' ),
		),
		'description'       => esc_html__( 'Style of the Button.', 'et_builder' ),
		),

		'button_size' => array(
		'label'             => esc_html__( 'Button Size', 'et_builder' ),
		'type'              => 'select',
		'option_category'   => 'configuration',
		'options'           => array(
		'small' => esc_html__( 'Small', 'et_builder' ),
		'medium'  => esc_html__( 'Medium', 'et_builder' ),
		'large'  => esc_html__( 'Large', 'et_builder' ),
		),
		'description'       => esc_html__( 'Size of the Button.', 'et_builder' ),
		),

		'button_orientation' => array(
		'label'             => esc_html__( 'Alignment of the Button', 'et_builder' ),
		'type'              => 'select',
		'option_category'   => 'configuration',
		'options'           => array(
		'left' => esc_html__( 'Left', 'et_builder' ),
		'right'  => esc_html__( 'Right', 'et_builder' ),
		'center'  => esc_html__( 'Center', 'et_builder' ),
		),
		'description'       => esc_html__( 'This controls the how Button is aligned within the module.', 'et_builder' ),
		),
		'text_orientation' => array(
		'label'             => esc_html__( 'Text Orientation', 'et_builder' ),
		'type'              => 'select',
		'option_category'   => 'layout',
		'options'           => et_builder_get_text_orientation_options(),
		'description'       => esc_html__( 'This controls the how your text is aligned within the module.', 'et_builder' ),
		),
		'disabled_on' => array(
		'label'           => esc_html__( 'Disable on', 'et_builder' ),
		'type'            => 'multiple_checkboxes',
		'options'         => array(
		'phone'   => esc_html__( 'Phone', 'et_builder' ),
		'tablet'  => esc_html__( 'Tablet', 'et_builder' ),
		'desktop' => esc_html__( 'Desktop', 'et_builder' ),
		),
		'additional_att'  => 'disable_on',
		'option_category' => 'configuration',
		'description'     => esc_html__( 'This will disable the module on selected devices', 'et_builder' ),
		),
		'admin_label' => array(
		'label'       => esc_html__( 'Admin Label', 'et_builder' ),
		'type'        => 'text',
		'description' => esc_html__( 'This will change the label of the module in the builder for easy identification.', 'et_builder' ),
		),
		'module_id' => array(
		'label'           => esc_html__( 'CSS ID', 'et_builder' ),
		'type'            => 'text',
		'option_category' => 'configuration',
		'tab_slug'        => 'custom_css',
		'option_class'    => 'et_pb_custom_css_regular',
		),
		'module_class' => array(
		'label'           => esc_html__( 'CSS Class', 'et_builder' ),
		'type'            => 'text',
		'option_category' => 'configuration',
		'tab_slug'        => 'custom_css',
		'option_class'    => 'et_pb_custom_css_regular',
		),
		);
		return $fields;
	}
	function shortcode_callback( $atts, $content = null, $function_name ) {
		$button_text            = $this->shortcode_atts['button_text'];
		$button_size 			= $this->shortcode_atts['button_size'];
		$button_url            = $this->shortcode_atts['button_url'];
		$button_style            = $this->shortcode_atts['button_style'];
		$button_size            = $this->shortcode_atts['button_size'];
		$button_orientation            = $this->shortcode_atts['button_orientation'];
		$button_theme            = $this->shortcode_atts['button_theme'];



		$module_id            = $this->shortcode_atts['module_id'];
		$module_class         = $this->shortcode_atts['module_class'];
		$module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );
		$this->shortcode_content = et_builder_replace_code_content_entities( $this->shortcode_content );
		if ( is_rtl() && 'left' === $text_orientation ) {
			$text_orientation = 'right';
		}
		$class = "skh_module skh_module--{$button_orientation} skhButton--{$button_theme}";
		$output = sprintf(
			'<div%3$s class="%2$s%4$s">
            	<a href="%8$s" class="skhButton skhButton--%7$s skhButton--size-%6$s skhButton--border-medium skhButton--text-thin skhButton--round-s" >%5$s</a>
            </div> <!-- Skh-Button-->',
		$this->shortcode_content,
		esc_attr( $class ),
		( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
		( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' ),
		$button_text,
		$button_size,
		$button_style,
		$button_url
		);
		return $output;
	}
}
new ET_Builder_Module_Elegant_Button;
