<?php




/**
* Add Admin Scripts
* Register all of the hooks related to the admin area functionality
* of the plugin.
*
*/
function skh_elegant_button_admin_scripts() {

    //Css
    wp_enqueue_style('skh_elegant_button_admin-css', SKH_E_BUTTON_PLUGIN_DRI . 'admin/css/skh-elegant-button-admin.css');

    //Js
    wp_enqueue_script('skh_elegant_button_admin-js', SKH_E_BUTTON_PLUGIN_DRI . 'admin/js/skh-elegant-button-admin.js', array('jquery'), '1.0', true);
}
add_action('admin_enqueue_scripts', 'skh_elegant_button_admin_scripts');




/**
* Add Public Scripts
* Register all of the hooks related to the public area functionality
* of the plugin.
*
*/
function skh_elegant_button_public_scripts() {

    //Css
    wp_enqueue_style('skh_elegant_button_public-css', SKH_E_BUTTON_PLUGIN_DRI . 'public/css/skh-elegant-button-public.css');

    //Js
    wp_enqueue_script('skh_elegant_button_public-js', SKH_E_BUTTON_PLUGIN_DRI . 'public/js/skh-elegant-button-public.js', array('jquery'), '1.0', true);
}
add_action('wp_enqueue_scripts', 'skh_elegant_button_public_scripts');