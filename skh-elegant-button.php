<?php
/**
 *
 * @wordpress-plugin
 * Plugin Name:       Elegant Button
 * Plugin URI:        www.shekhkamal.com
 * Description:       Collection of awesome button.
 * Version:           1.0.0
 * Author:            Shekh Kamal Hussain
 * Author URI:        www.shekhkamal.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       skh-elegant-button
 * Domain Path:       /languages
 */



/**
* SET UP =================================
*/


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'SKH_E_BUTTON_PLUGIN_DRI', plugin_dir_url(__FILE__) );


/**
* INCLUDES =================================
*/
require_once plugin_dir_path( __FILE__ ) . 'includes/load-scripts.php';



/**
* HOOKS =================================
*/

/**
 * The code that runs during plugin activation.
 */
function skh_elegant_button_activate(){
	require_once plugin_dir_path( __FILE__ ) . 'includes/activator.PHP';
}

/**
 * The code that runs during plugin deactivation.
 */
function skh_elegant_button_deactivate(){
	require_once plugin_dir_path( __FILE__ ) . 'includes/deactivator.php';
}

/**
 * The code that runs when plugin initialize
 */
function skh_elegant_button_init(){
	require_once plugin_dir_path( __FILE__ ) . 'includes/init.php';
}


register_activation_hook( __FILE__, 'skh_elegant_button_activate');

add_action('init','skh_elegant_button_init');

register_deactivation_hook( __FILE__, 'skh_elegant_button_deactivate');



/**
* SHORTCODES =================================
*/